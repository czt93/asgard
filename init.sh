#!/bin/bash

echo Initializing minikube...
minikube start
echo Successfully initialized minikube!

# creating directories for mysql-pv
mkdir -p volume/mysql

# mounts existing ./volume to control plane for PV and PVC use
echo Mounting /volume to /mnt/vol...
nohup minikube mount volume:/mnt/vol &
echo Volume successfully mounted!

echo Spinning up k8s resources using yaml found in $PWD...
kubectl apply -f $PWD
echo Completed creating resources!

# display minikube VM address for convenience purposes
kubectl cluster-info