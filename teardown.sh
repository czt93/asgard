#!/bin/bash

# assuming name of resource is always declared after metadata tag

# convenience function to dynamically delete resources
# del_resoc "file_name" 
function del_resoc() {
    IFS=" "
    type=$(echo $1 | cut -d"." -f1)
    items=( $(grep -A1 "metadata" "$1" | grep name | cut -d":" -f2 | cut -d" " -f2 ))
    readarray -t arr <<< "$items"
    for item in "${arr[@]}";
    do
        kubectl delete $type $item
    done;
}

arr=("deployments" "configmaps" "services" "pvc" "pv")

for res in "${arr[@]}";
do
    echo -e "Deleting $res...\n------"
    del_resoc "$res.yaml"
    echo -e "------\n$res deleted! \n"
done;

# find mount process and kill
sudo kill -9 $(ps aux | grep "[m]inikube mount" | tr -s " " | cut -d" " -f2)
echo "Killed minikube mount process"

minikube stop
echo "Stopped minikube"