# Introduction
Take-home assignment Scenario 2 -- spinning up an application from Scenario 1, have it talking to a mysql database running in the same cluster with Prometheus + Grafana as monitoring.

## Table of content
- [Assumptions](#assumptions)
- [Scripts](#scripts)
- [Counter application & connectivity](#connectivity-check-for-application---database)
- [Installing Prometheus & Grafana](#installing-prometheus--grafana)

## Assumptions
- Running on a linux distribution (commands listed thereafter are executed on Arch Linux)
- `minikube`, `kubectl`, `helm` already installed
- Yaml fields follow a certain convention (metadata[0] == name)

## Scripts

There are 2 scripts in this repository -- mainly `init.sh` and `teardown.sh`.  

`init.sh` starts the minikube cluster and mounts the `/volume` folder to the `/mnt/vol` mount point in the minikube VM; thereafter spinning up the declared resources in the current folder this script is being ran from.  

`teardown.sh` tears down all k8s resources by dynamically looking up the metadata name of each resource; thereafter killing the mount process and stopping the minikube VM.


## Counter application and connectivity

Accessing nginx-deployment pod:  
```shell
kubectl exec -it $(kubectl get pods | grep nginx-deployment | tr -s " " | cut -d" " -f1) -- /bin/bash
```

Verifying environment variable has been passed through:  
![env.png](assets/env.png)

Verifying that counter application reflects correctly and is working:  
![app.png](assets/app.png)

Updating + getting mysql-client installed on the pod:  
```shell
apt update && apt install mysql-client -y
```

Connect to the mysql database:  
```shell
mysql -h mysql-service -P 3306 -u root -p
```

Screenshot for the creation of database/table as below:  
![connectivity.png](assets/connectivity.png)

## Installing Prometheus & Grafana

Adding repositories to helm:  
```shell
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add grafana https://grafana.github.io/helm-charts
```

Installing via helm:  
```shell
helm install prometheus prometheus-community/prometheus
helm install grafana grafana/grafana
```

Additionally, to track mysql db metrics this the exporter can be installed:  
```shell
helm install prometheus-mysql-exporter prometheus-community/prometheus-mysql-exporter -f values.yaml
```

Exposing prometheus-server port:  
```shell
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext
```

Sample of a query on prometheus server checking for deployment containers:  
![prometheus.png](assets/prometheus.png)


Retrieving credentials for grafana (password, user):  
```shell
# Cg== for base64 encoding of newline
kubectl get secret grafana -o yaml | grep admin | cut -d":" -f2 | cut -d" " -f2 | xargs -i echo {}Cg== | base64 -d
```

These credentials can then be used to access Grafana via the UI.  
Example of a Prometheus 2.0 dashboard (https://grafana.com/grafana/dashboards/3662-prometheus-2-0-overview/):  
![grafana.png](assets/grafana.png)


To uninstall respective helm charts:  
```shell
helm uninstall prometheus-community/prometheus
```
